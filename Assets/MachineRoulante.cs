﻿using UnityEngine;
using System.Collections;

public class MachineRoulante : MonoBehaviour {
	public Vector3 spawn;
	public Vector3 point1;
	public Vector3 direction1;
	GameObject g1;
	GameObject g2;
	public bool sens1;
	// Use this for initialization
	void Start () {
		spawn = new Vector3 (-1, -0.6f, 0.48f);
		point1 = new Vector3 (-3.33f, -0.6f, 0.76f);
		direction1 = direction (spawn, point1);
		g1=Instantiate (Resources.Load ("Objet")) as GameObject;
		g1.transform.parent = transform;
		g1.transform.localPosition = spawn;
		sens1 = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (sens1) {
			if (distance (g1.transform.localPosition, point1) > 0.01) {
				g1.transform.Rotate (0, Time.deltaTime * 10, 0);
				g1.transform.localPosition = new Vector3 (g1.transform.localPosition.x + direction1.x / 10 * Time.deltaTime, g1.transform.localPosition.y + direction1.y / 10 * Time.deltaTime, g1.transform.localPosition.z + direction1.z / 10 * Time.deltaTime);
		
			}
			else{
				g1.transform.localPosition=point1;
			}
		}
	}


	public Vector3 direction(Vector3 v1,Vector3 v2){
		return new Vector3 (v2.x - v1.x, v2.y - v1.y, v2.z - v1.z);

	}

	public float distance(Vector3 v1,Vector3 v2){
		return Mathf.Sqrt((v2.x - v1.x)*(v2.x - v1.x)+(v2.y - v1.y)*(v2.y - v1.y)+(v2.z - v1.z)*(v2.z - v1.z));
	}
}
