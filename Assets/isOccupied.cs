﻿using UnityEngine;
using System.Collections;

public class isOccupied : MonoBehaviour {
	public bool occup;
	public GameObject networ;
	public bool sendEtat;

	// Use this for initialization
	void Start () {
		networ = GameObject.Find ("network");
		occup = false;
		sendEtat = false;
	}

	// Update is called once per frame
	void Update () {
		if (occup) {
			networ.transform.GetComponent<NetworkManager> ().etatTable(gameObject.name);

		}
		if (sendEtat) {
			int gauche1=0;
			if(gameObject.transform.GetChild(0).GetChild(0)){
				gauche1=gameObject.transform.GetChild(0).GetChild(0).GetComponent<tk2dSlicedSprite>().spriteId;
			}
			int gauche2=0;
			if(gameObject.transform.GetChild(1).GetChild(0)){
				gauche2=gameObject.transform.GetChild(1).GetChild(0).GetComponent<tk2dSlicedSprite>().spriteId;
			}
			int centre=0;
			if(gameObject.transform.GetChild(2).GetChild(0)){
				centre=gameObject.transform.GetChild(2).GetChild(0).GetComponent<tk2dSlicedSprite>().spriteId;
			}
			int droite1=0;
		
			if(gameObject.transform.GetChild(3).GetChild(0)){
				droite1=gameObject.transform.GetChild(3).GetChild(0).GetComponent<tk2dSlicedSprite>().spriteId;
			}
			int droite2=0;
			
			if(gameObject.transform.GetChild(4).GetChild(0)){
				droite2=gameObject.transform.GetChild(4).GetChild(0).GetComponent<tk2dSlicedSprite>().spriteId;
			}
			int entree=int.Parse(gameObject.transform.GetChild(5).GetChild(0).GetChild(0).GetComponent<tk2dTextMesh>().text);
			int sortie=int.Parse(gameObject.transform.GetChild(5).GetChild(0).GetChild(1).GetComponent<tk2dTextMesh>().text);
			networ.transform.GetComponent<NetworkManager> ().situationTable(gameObject.name,gauche1,gauche2,centre,droite1,droite2,entree,sortie);

		}


	}

	public Vector3 sortie(){
		return gameObject.transform.GetChild (6).position;
	}
}
