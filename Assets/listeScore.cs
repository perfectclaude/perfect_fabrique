﻿using UnityEngine;
using System.Collections;

public class listeScore : MonoBehaviour {

	
	public GameObject networ;
	public GameObject infoserv;
	public bool refresh;
	
	void Start(){
		networ = GameObject.Find ("network");


		refresh = false;
	}
	// Use this for initialization
	void Update() {

		if(refresh){
			int i=0;
		foreach (string nom in networ.transform.GetComponent<NetworkManager> ().noms) {
				if(networ.transform.GetComponent<NetworkManager> ().scores[networ.transform.GetComponent<NetworkManager> ().noms.IndexOf(nom)]>-1){
					Destroy(GameObject.Find("Score"+nom));
					GameObject nouv = Instantiate (Resources.Load ("ScoreJoueur")) as GameObject;
					nouv.transform.parent = gameObject.transform;

					if (i < 5) {
						nouv.transform.localPosition = new Vector3 (0.5f, 3.6f - 0.7f * (i ), -0.01f);
					} else {
						nouv.transform.localPosition = new Vector3 (4.6f, 3.6f - 0.7f * (i - 5), -0.01f);
					}
					nouv.transform.localScale=new Vector3(1,1,1);
					nouv.name = "Score"+nom;
					nouv.transform.GetChild (0).GetComponent<tk2dTextMesh> ().text = nom;
					nouv.transform.GetChild (1).GetComponent<tk2dTextMesh> ().text = networ.transform.GetComponent<NetworkManager> ().scores[i].ToString();


					i++;
				}

			}
			refresh=false;
		}
		
	}
}
