﻿using UnityEngine;
using System.Collections;

public class ListeServeurOcculus : MonoBehaviour {

	public GameObject networ;
	int position;
	public bool refresh;
	
	void Start(){
		position = 0;
		networ = GameObject.Find ("network");
		refresh = false;
	}
	// Use this for initialization
	void Update() {
		networ.transform.GetComponent<NetworkManager> ().RefreshHostList ();
		if (Input.GetKey ("UP")) {
			if(position==networ.transform.GetComponent<NetworkManager>().hostList.Length-1){
				position=0;
			}
			else{
				position++;
			}
		}
		if (Input.GetKey ("Down")) {
			if(position==0){
				position=networ.transform.GetComponent<NetworkManager>().hostList.Length-1;
			}
			else{
				position--;
			}
		}
		if (refresh) {
			
			
			int i = 0;
			foreach (HostData data in networ.transform.GetComponent<NetworkManager>().hostList) {
				GameObject nouv = Instantiate (Resources.Load ("AfficheServerOcculus")) as GameObject;
				nouv.transform.parent = gameObject.transform;
				nouv.transform.localPosition = new Vector3 (0.0f, 3.6f - 0.9f * i, -0.01f);
				nouv.name = "host" + i.ToString ();
				nouv.transform.GetChild(0).GetComponent<tk2dTextMesh>().text=data.gameName;
				nouv.transform.GetChild (1).GetComponent<joinNet> ().hostnum = i;
				if (i % 2 == 1) {
					nouv.transform.GetComponent<tk2dSprite> ().color = new Color (0.29f, 0.42f, 0.58f);
				}
				i++;
				
			}
			refresh=false;
		}
	}
}
