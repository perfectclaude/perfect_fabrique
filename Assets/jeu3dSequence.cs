﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class jeu3dSequence : MonoBehaviour {

	public int position;
	public int nombreFait;
	public List<string> sequence;
	bool test;
	public bool chang;
	GameObject avant1;
	GameObject avant2;
	GameObject apres1;
	GameObject apres2;
	GameObject centre;
	GameObject information;

	public float wait ;
	public float tmps;
	public GameObject networ;
	public bool fin;
	bool init;
	
	// Use this for initialization
	void Start () {
		position = 0;
		nombreFait = 0;
		initsequence ();
		test = true;
		chang = false;
		fin = true;
		init = false;



		networ = GameObject.Find ("network");
		wait = 0.5f;
		tmps = 0.0f;

	}

	public void editcentre(GameObject gam){

		centre = gam.transform.GetChild(2).GetChild(0).gameObject;
		avant1 = gam.transform.GetChild(0).GetChild(0).gameObject;
		avant2 = gam.transform.GetChild(1).GetChild(0).gameObject;
		apres1 = gam.transform.GetChild(3).GetChild(0).gameObject;
		apres2 = gam.transform.GetChild(4).GetChild(0).gameObject;
		information=gam.transform.GetChild(5).GetChild(0).gameObject;
		init = true;
	}
	public void initsequence(){
		sequence = new List<string> ();
		int rand=(int)(Random.value*10);
		
		for (int i=0; i<(rand+5); i++) {
			float val=Random.value;
			if(val>0.75f){
				sequence.Add ("r");
			}
			else if(val>0.5f){
				sequence.Add ("e");
			}
			else if(val>0.25f){
				sequence.Add ("z");
			}
			else{
				sequence.Add ("a");
			}
		}
	}
	public int formeassocie(string lettre){
		switch (lettre) {
		case "a":
			return 22;
		break;
		case "z":
			return 23;
		break;
		case "e":
			return 24;
		break;
		case "r":
			return 25;
		break;
		default:
				return 0;
		break;
		}
	}
	void affichersequence ()
	{
		if (position == 0) {
			avant1.SetActive (false);
			avant2.SetActive (false);
			apres1.SetActive (true);
			apres2.SetActive (true);
			centre.transform.parent.GetChild(1).GetComponent<tk2dSlicedSprite> ().color = new Color (0.71f, 0.24f, 0.24f);
			centre.GetComponent<tk2dSlicedSprite> ().spriteId = formeassocie(sequence [position]);
			apres1.GetComponent<tk2dSlicedSprite> ().spriteId = formeassocie(sequence [position + 1]);
			apres2.GetComponent<tk2dSlicedSprite> ().spriteId = formeassocie(sequence [position + 2]);

			
			
		} else if (position == 1) {
			avant1.SetActive (false);
			avant2.SetActive (true);
			apres1.SetActive (true);
			apres2.SetActive (true);
			centre.transform.parent.GetChild(1).GetComponent<tk2dSlicedSprite> ().color = new Color (0.71f, 0.24f, 0.24f);
			centre.transform.GetComponent<tk2dSlicedSprite> ().spriteId = formeassocie(sequence [position]);
			apres1.transform.GetComponent<tk2dSlicedSprite> ().spriteId = formeassocie(sequence [position + 1]);
			apres2.transform.GetComponent<tk2dSlicedSprite> ().spriteId = formeassocie(sequence [position + 2]);
			avant2.transform.GetComponent<tk2dSlicedSprite> ().spriteId = formeassocie(sequence [position - 1]);
			
			
		} else if (position == sequence.Count-1 ) {
			avant1.SetActive (true);
			avant2.SetActive (true);
			apres1.SetActive (false);
			apres2.SetActive (false);
			centre.transform.parent.GetChild(1).GetComponent<tk2dSlicedSprite> ().color = new Color (0.71f, 0.24f, 0.24f);
			centre.transform.GetComponent<tk2dSlicedSprite> ().spriteId = formeassocie(sequence [position]);
			avant2.transform.GetComponent<tk2dSlicedSprite> ().spriteId = formeassocie(sequence [position - 1]);

			avant1.transform.GetComponent<tk2dSlicedSprite> ().spriteId = formeassocie(sequence [position - 2]);
			
		} else if (position == sequence.Count - 2) {
			avant1.SetActive (true);
			avant2.SetActive (true);
			apres1.SetActive (true);
			apres2.SetActive (false);
			centre.transform.parent.GetChild(1).GetComponent<tk2dSlicedSprite> ().color = new Color (0.71f, 0.24f, 0.24f);
			centre.transform.GetComponent<tk2dSlicedSprite> ().spriteId = formeassocie(sequence [position]);
			apres1.transform.GetComponent<tk2dSlicedSprite> ().spriteId = formeassocie(sequence [position + 1]);
			avant2.transform.GetComponent<tk2dSlicedSprite> ().spriteId = formeassocie(sequence [position - 1]);
			avant1.transform.GetComponent<tk2dSlicedSprite> ().spriteId = formeassocie(sequence [position - 2]);

			
			
		} else {
			avant1.SetActive (true);
			avant2.SetActive (true);
			apres1.SetActive (true);
			apres2.SetActive (true);
			centre.transform.parent.GetChild(1).GetComponent<tk2dSlicedSprite> ().color = new Color (0.71f, 0.24f, 0.24f);
			centre.transform.GetComponent<tk2dSlicedSprite> ().spriteId = formeassocie(sequence [position]);
			apres1.transform.GetComponent<tk2dSlicedSprite> ().spriteId = formeassocie(sequence [position + 1]);
			apres2.transform.GetComponent<tk2dSlicedSprite> ().spriteId = formeassocie(sequence [position + 2]);
			avant2.transform.GetComponent<tk2dSlicedSprite> ().spriteId = formeassocie(sequence [position - 1]);
			avant1.transform.GetComponent<tk2dSlicedSprite> ().spriteId = formeassocie(sequence [position - 2]);

		}
	}
	
	// Update is called once per frame
	void Update () {

		if (Network.isServer) {
			init=true;
		}
		if (init) {
			Debug.Log(information.name);
			information.transform.GetChild(0).GetComponent<tk2dTextMesh>().text=networ.transform.GetComponent<NetworkManager> ().entree.ToString();
			information.transform.GetChild(1).GetComponent<tk2dTextMesh>().text=networ.transform.GetComponent<NetworkManager> ().sortie.ToString();
			if (tmps > wait) {
			
				if (chang) {
					if (position < (sequence.Count)) {
						centre.SetActive (true);
						affichersequence ();
					
					} else {
						centre.SetActive (false);
						avant1.SetActive (false);
						avant2.SetActive (false);
						apres1.SetActive (false);
						apres2.SetActive (false);
						nombreFait++;
						fin = true;

						networ.transform.GetComponent<NetworkManager>().finsequence();
						networ.transform.GetComponent<NetworkManager>().envoyerscore(nombreFait);
					}
					chang = false;
				}
				if (position < sequence.Count && !fin) {
					if (Input.GetKey (sequence [position])) {
					
						position++;
						centre.transform.parent.GetChild(1).GetComponent<tk2dSlicedSprite> ().color = new Color (0.19f, 0.84f, 0.29f);
					
						tmps = 0;
						chang = true;
					
					
					}
				
				}
				if (fin) {
					Debug.Log("hi");
					networ.transform.GetComponent<NetworkManager>().nouvellesequence();
				
				
				
				}
			} else {
				tmps += Time.deltaTime;
			}
		}
	}
	
	
	public void restart(){

		position=0;
		tmps=0;
		fin = false;
		chang = true;
		networ.transform.GetComponent<NetworkManager>().envoyerscore(nombreFait);
	}
}
