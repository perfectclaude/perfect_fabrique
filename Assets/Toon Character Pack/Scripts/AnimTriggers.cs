using UnityEngine;
using System.Collections;

public class AnimTriggers : MonoBehaviour
{	
	// Create a reference to the animator component
	private Animator animator;
	public bool gui;
	public GameObject Entrer;
	public GameObject Quitter;
	public bool entrer;
	GameObject colider;
	public bool fin;
	public GameObject networ;


	void Awake(){
		networ = GameObject.Find ("network");

			Entrer = gameObject.transform.GetChild (0).gameObject;
			Quitter = gameObject.transform.GetChild (1).gameObject;
			Entrer.SetActive (false);
			entrer = false;
			Quitter.SetActive (false);


	}
	void Start ()
	{
		// initialise the reference to the animator component

		gui = false;
		animator = gameObject.GetComponent<Animator>();
	}
	void OnCollisionEnter (Collision col)
	{
		if (!networ.transform.GetComponent<NetworkManager> ().spectateur && !fin) {

			if (col.gameObject.name.Substring (0, 5) == "table" && col.gameObject.transform.GetComponent<isOccupied> ().occup == false) {
				colider = col.gameObject;
				Entrer.SetActive (true);
				entrer = true;
			}
		}

		
	}

	void OnCollisionExit (Collision col)
	{
		if (col.gameObject.name.Substring (0, 5) == "table" && col.gameObject.transform.GetComponent<isOccupied> ().occup == false) {
			colider = col.gameObject;
			Entrer.SetActive (false);
			entrer = false;

		}
		
	}
	// check for colliders with a Trigger collider
	// if we are entering something called JumpTrigger, set a bool parameter called JumpDown to true..
	void OnTriggerEnter(Collider col)
	{
		Debug.Log ("salut");
		if(col.gameObject.name == "JumpTrigger")
		{
			animator.SetBool("JumpDown", true);	
		}
	}

	public void sortie(){
		if (!networ.transform.GetComponent<NetworkManager> ().spectateur) {
			gameObject.transform.position=colider.transform.GetComponent<isOccupied>().sortie();
			fin=true;
			gameObject.GetComponent<BotControlScript>().animSpeed=1.5f;
			gameObject.GetComponent<BotControlScript>().lookSmoother=1.5f;
			Quitter.SetActive(true);
		}
	}
	
	// ..and when leaving the trigger, reset it to false
	void OnTriggerExit(Collider col)
	{
		if(col.gameObject.name == "JumpTrigger")
		{
			animator.SetBool("JumpDown", false);
		}
	}


	void Update(){
		if (entrer) {
			if (Input.GetKey ("a")) {
				networ.transform.GetComponent<NetworkManager> ().envoyerEtat();
				Entrer.SetActive(false);
				Destroy(colider.GetComponent<BoxCollider>());
				gameObject.GetComponent<BotControlScript>().animSpeed=0;
				gameObject.GetComponent<BotControlScript>().lookSmoother=0;
				if(colider.name=="tableJeu1"){
					gameObject.transform.position=new Vector3(-4.5f,0,5.3f);
					gameObject.transform.rotation=new Quaternion(0,0,0,0);
				}
				if(colider.name=="tableJeu2"){
					gameObject.transform.position=new Vector3(-4.5f,0,9.6f);
					gameObject.transform.rotation=new Quaternion(0,180,0,0);
				}
				if(colider.name=="tableJeu3"){
					gameObject.transform.position=new Vector3(3.1f,0,5.8f);
					gameObject.transform.rotation=new Quaternion(0,0,0,0);
				}
				if(colider.name=="tableJeu4"){
					gameObject.transform.position=new Vector3(3.1f,0,9.2f);
					gameObject.transform.rotation=new Quaternion(0,180,0,0);
				}
				colider.transform.GetComponent<isOccupied>().occup=true;
				if(gameObject.name==networ.transform.GetComponent<NetworkManager> ().nomjoueur){
					colider.transform.GetComponent<isOccupied>().sendEtat=true;
				}
				gameObject.GetComponent<jeu3dSequence>().editcentre(colider);
			}

		}
	}

}
