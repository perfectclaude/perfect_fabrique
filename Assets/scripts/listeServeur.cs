﻿using UnityEngine;
using System.Collections;

public class listeServeur : MonoBehaviour {
	public GameObject networ;

	public bool refresh;
	float temps;
	void Start(){
		networ = GameObject.Find ("network");
		refresh = false;
		temps = 0;
	}
	// Use this for initialization
	void Update() {
		temps += Time.deltaTime;
		if (temps > 2) {
			temps = 0;
			networ.transform.GetComponent<NetworkManager> ().RefreshHostList ();
			refresh = true;
		}
		if (refresh) {


			int i = 0;
			foreach (HostData data in networ.transform.GetComponent<NetworkManager>().hostList) {
				foreach(Transform T in gameObject.transform){
					if(T.gameObject.name.Substring (0,4)=="host"){
						Destroy(T.gameObject);
					}
				}
				GameObject nouv = Instantiate (Resources.Load ("AfficheServer")) as GameObject;
				nouv.transform.parent = gameObject.transform;
				nouv.transform.localPosition = new Vector3 (0.0f, 3.6f - 0.9f * i, -0.01f);
				nouv.name = "host" + i.ToString ();
				nouv.transform.GetChild(0).GetComponent<tk2dTextMesh>().text=data.gameName;
				nouv.transform.GetChild (1).GetComponent<joinNet> ().hostnum = i;
				if (i % 2 == 1) {
					nouv.transform.GetComponent<tk2dSprite> ().color = new Color (0.29f, 0.42f, 0.58f);
				}
				i++;

			}
			refresh=false;
		}
	}
	

}
