﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class jeuSequence : MonoBehaviour {
	public int position;
	public int nombreFait;
	public List<string> sequence;
	bool test;
	public bool chang;
	GameObject avant1;
	GameObject avant2;
	GameObject apres1;
	GameObject apres2;
	GameObject centre;
	GameObject cree;
	GameObject entree;
	GameObject sortie;
	public float wait ;
	public float tmps;
	public GameObject networ;
	public bool fin;


	// Use this for initialization
	void Start () {
		position = 0;
		nombreFait = 0;
		initsequence ();
		test = true;
		chang = false;
		fin = true;
		centre = GameObject.Find ("boutonCentral");
		avant1 = GameObject.Find ("boutonliste0");
		avant2 = GameObject.Find ("boutonliste1");
		apres1 = GameObject.Find ("boutonliste2");
		apres2 = GameObject.Find ("boutonliste3");
		cree = GameObject.Find ("nbrcree");
		entree = GameObject.Find ("nbrentree");
		sortie = GameObject.Find ("nbrsorties");
		networ = GameObject.Find ("network");
		wait = 0.5f;
		tmps = 0.0f;
		centre.SetActive(false);
		avant1.SetActive (false);
		avant2.SetActive (false);
		apres1.SetActive (false);
		apres2.SetActive (false);
	}
	
	public void initsequence(){
		sequence = new List<string> ();
		int rand=(int)(Random.value*10);

		for (int i=0; i<(rand+5); i++) {
			float val=Random.value;
			if(val>0.75f){
				sequence.Add ("r");
			}
			else if(val>0.5f){
				sequence.Add ("e");
			}
			else if(val>0.25f){
				sequence.Add ("z");
			}
			else{
				sequence.Add ("a");
			}
		}
	}

	void affichersequence ()
	{
		if (position == 0) {
			avant1.SetActive (false);
			avant2.SetActive (false);
			apres1.SetActive (true);
			apres2.SetActive (true);
			gameObject.transform.GetChild (0).GetComponent<tk2dSprite> ().color = new Color (0.71f, 0.24f, 0.24f);
			gameObject.transform.GetChild (0).GetChild (0).GetComponent<tk2dTextMesh> ().text = sequence [position];
			apres1.transform.GetChild (0).GetComponent<tk2dTextMesh> ().text = sequence [position + 1];
			apres2.transform.GetChild (0).GetComponent<tk2dTextMesh> ().text = sequence [position + 2];
			

		} else if (position == 1) {
			avant1.SetActive (false);
			avant2.SetActive (true);
			apres1.SetActive (true);
			apres2.SetActive (true);
			gameObject.transform.GetChild (0).GetComponent<tk2dSprite> ().color = new Color (0.71f, 0.24f, 0.24f);
			gameObject.transform.GetChild (0).GetChild (0).GetComponent<tk2dTextMesh> ().text = sequence [position];
			apres1.transform.GetChild (0).GetComponent<tk2dTextMesh> ().text = sequence [position + 1];
			apres2.transform.GetChild (0).GetComponent<tk2dTextMesh> ().text = sequence [position + 2];
			avant2.transform.GetChild (0).GetComponent<tk2dTextMesh> ().text = sequence [position - 1];


		} else if (position == sequence.Count-1 ) {
			avant1.SetActive (true);
			avant2.SetActive (true);
			apres1.SetActive (false);
			apres2.SetActive (false);
			gameObject.transform.GetChild (0).GetComponent<tk2dSprite> ().color = new Color (0.71f, 0.24f, 0.24f);
			gameObject.transform.GetChild (0).GetChild (0).GetComponent<tk2dTextMesh> ().text = sequence [position];
			avant1.transform.GetChild (0).GetComponent<tk2dTextMesh> ().text = sequence [position - 1];
			avant2.transform.GetChild (0).GetComponent<tk2dTextMesh> ().text = sequence [position - 2];
			
			
		} else if (position == sequence.Count - 2) {
			avant1.SetActive (true);
			avant2.SetActive (true);
			apres1.SetActive (true);
			apres2.SetActive (false);
			gameObject.transform.GetChild (0).GetComponent<tk2dSprite> ().color = new Color (0.71f, 0.24f, 0.24f);
			gameObject.transform.GetChild (0).GetChild (0).GetComponent<tk2dTextMesh> ().text = sequence [position];
			apres1.transform.GetChild (0).GetComponent<tk2dTextMesh> ().text = sequence [position + 1];
			avant1.transform.GetChild (0).GetComponent<tk2dTextMesh> ().text = sequence [position - 1];
			avant2.transform.GetChild (0).GetComponent<tk2dTextMesh> ().text = sequence [position - 2];
			
			
		} else {
			avant1.SetActive (true);
			avant2.SetActive (true);
			apres1.SetActive (true);
			apres2.SetActive (true);
			gameObject.transform.GetChild (0).GetComponent<tk2dSprite> ().color = new Color (0.71f, 0.24f, 0.24f);
			gameObject.transform.GetChild (0).GetChild (0).GetComponent<tk2dTextMesh> ().text = sequence [position];
			apres1.transform.GetChild (0).GetComponent<tk2dTextMesh> ().text = sequence [position + 1];
			apres2.transform.GetChild (0).GetComponent<tk2dTextMesh> ().text = sequence [position + 2];
			avant1.transform.GetChild (0).GetComponent<tk2dTextMesh> ().text = sequence [position - 1];
			avant2.transform.GetChild (0).GetComponent<tk2dTextMesh> ().text = sequence [position - 2];
		}
	}

	// Update is called once per frame
	void Update () {

		entree.transform.GetComponent<tk2dTextMesh>().text=networ.transform.GetComponent<NetworkManager> ().entree.ToString();
		sortie.transform.GetComponent<tk2dTextMesh>().text=networ.transform.GetComponent<NetworkManager> ().sortie.ToString();
		if (tmps > wait) {

			if (chang) {
				if (position < (sequence.Count )) {
				centre.SetActive(true);
				affichersequence ();

				}
				else{
					centre.SetActive(false);
					avant1.SetActive (false);
					avant2.SetActive (false);
					apres1.SetActive (false);
					apres2.SetActive (false);
					nombreFait++;
					fin=true;
					cree.GetComponent<tk2dTextMesh>().text=nombreFait.ToString();
					networ.transform.GetComponent<NetworkManager>().finsequence();

				}
				chang = false;
			}
			if (position < sequence.Count && !fin ) {
				if (Input.GetKey (sequence [position])) {

						position++;
						gameObject.transform.GetChild (0).GetComponent<tk2dSprite> ().color = new Color (0.19f, 0.84f, 0.29f);
						
						tmps = 0;
						chang = true;


				}
			
			}
			if (fin){
				networ.transform.GetComponent<NetworkManager>().nouvellesequence();

				
				
			}
		} 
		else {
				tmps += Time.deltaTime;
			}
		}
	

	public void restart(){
		Debug.Log ("restart");
		position=0;
		tmps=0;
		fin = false;
		chang = true;
	}

}
