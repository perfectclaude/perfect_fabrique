﻿using UnityEngine;
using System.Collections;

public class ListeNom : MonoBehaviour {

	public GameObject networ;
	public GameObject infoserv;
	public bool refresh;
	
	void Start(){
		networ = GameObject.Find ("network");

		refresh = true;
	}
	// Use this for initialization
	void Update() {
		refresh = networ.transform.GetComponent<NetworkManager> ().rafraich;

		if (refresh) {
			foreach (Transform t in transform) {
				Destroy (t.gameObject);
			}
			int i = 0;
		
			foreach (string nom in networ.transform.GetComponent<NetworkManager> ().noms) {
				GameObject nouv = Instantiate (Resources.Load ("NomJoueur")) as GameObject;
				nouv.transform.parent = gameObject.transform;
				if (networ.transform.GetComponent<NetworkManager> ().pret[i]) {
					nouv.transform.GetChild (1).GetComponent<tk2dTextMesh> ().color = Color.green;
				}
				if (i < 5) {
					nouv.transform.localPosition = new Vector3 (-2.8f, 1.6f - 0.8f * (i ), -0.01f);
				} else {
					nouv.transform.localPosition = new Vector3 (2.8f, 1.6f - 0.7f * (i - 5), -0.01f);
				}
				nouv.transform.localScale=new Vector3(1,1,1);
				nouv.name = "nom: "+nom;
				nouv.transform.GetChild (0).GetComponent<tk2dTextMesh> ().text = nom;
				networ.transform.GetComponent<NetworkManager> ().rafraich=false;
				i++;
				
			}
			refresh=false;
		}
			
		}

}
