﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System;
using BestHTTP;

public class networkfiles : MonoBehaviour
{
	public static string localfilespath = Application.persistentDataPath + "/d2cfiles";
	public static byte [] Data = null;
	public static string DataStr = null;
	public static string [] Filedir = null;

	static bool _SaveFile = false;
	static bool _LoadFile = false;
	static string _LoadFile_path;
	static string _LoadFile_filename;
	public static bool networkfilesbusy = false;
	static bool _DirFile = false;
	static float _WaitForInternet = 0.0f;

	public static bool SaveFile(string path, string filename, string data)
	{
		Debug.Log (networkfilesbusy);
		if (networkfilesbusy)		return false;
		Data = null;
		DataStr = data;
		networkfilesbusy = true;
		_LoadFile_path = path;
		_LoadFile_filename = filename;
		_SaveFile = true;
		return true;
	}

	public static bool SaveFile(string path, string filename, byte [] data)
	{
		if (networkfilesbusy)		return false;
		Data = data;
		DataStr = null;
		networkfilesbusy = true;
		_LoadFile_path = path;
		_LoadFile_filename = filename;
		_SaveFile = true;
		return true;
	}

	public static bool LoadFile(string path, string filename)
	{
		if (networkfilesbusy)		return false;
		networkfilesbusy = true;
		_LoadFile_path = path;
		_LoadFile_filename = filename;
		_LoadFile = true;
		return true;
	}

	public static bool DirFile()
	{
		if (networkfilesbusy)		return false;
		networkfilesbusy = true;
		_DirFile = true;
		_WaitForInternet = 0.0f;
		return true;
	}

	public static bool DirFileWaitConnected()
	{
		if (networkfilesbusy)		return false;
		networkfilesbusy = true;
		_DirFile = true;
		_WaitForInternet = Time.time + 3.0f;
		return true;
	}

	void Awake ()
	{
//		LoadFile("bras","bras_obj.txt");
//		DirFile();
	}
	
	void Update ()
	{
		if (_LoadFile)
		{
			StopCoroutine("i_LoadFile");
			_LoadFile = false;
			StartCoroutine("i_LoadFile");
		}
		if (_SaveFile)
		{
			StopCoroutine("i_SaveFile");
			_SaveFile = false;
			StartCoroutine("i_SaveFile");
		}
		if (_DirFile)
		{
			StopCoroutine("i_DirFile");
			_DirFile = false;
			StartCoroutine("i_DirFile");
		}
	
	}

	IEnumerator i_DirFile()
	{
		while (Time.time < _WaitForInternet)
		{
			yield return new WaitForSeconds(0.03f);
			if (network.internetconnected)
				break;
		}
		if (network.internetconnected)
		{
			network.GetFileDir();
			while (network.waitingnetwork)
				yield return new WaitForSeconds(0.03f);
			if (network.returntext.Substring(network.returntext.Length-2,1) == ";")
			{
				network.returntext = network.returntext.Substring(0,network.returntext.Length-2);
			}
			Filedir = network.returntext.Split(';');
		}
		else
		{
// get local file dir
			string [] testlist = System.IO.Directory.GetDirectories(localfilespath);
			for (int i=0;i<testlist.Length;i++)
			{
				testlist[i] = testlist[i].Substring(testlist[i].LastIndexOf("/")+1);
			}
			Filedir = testlist;
		}
		networkfilesbusy = false;
//		for (int i=0;i<Filedir.Length;i++)
//			Debug.Log ("F:"+Filedir[i]);

	}


	IEnumerator i_SaveFile()
	{
		string targetpath = localfilespath;
		byte [] data = null;
		int i_servertimedate = 0;
		
		System.IO.Directory.CreateDirectory(targetpath);
		targetpath = targetpath + "/" + _LoadFile_path;
		System.IO.Directory.CreateDirectory(targetpath);
		targetpath = targetpath + "/" + _LoadFile_filename;

// Save locally with new date
		if (DataStr != null)	System.IO.File.WriteAllText(targetpath,DataStr);
		if (Data != null)		System.IO.File.WriteAllBytes(targetpath,Data);
		double milisec = network.GetMiliseconds(DateTime.Now);
		milisec = milisec / 1000.0;
		int savedate = (int)milisec;
		System.IO.File.WriteAllText(targetpath+"_date",savedate.ToString());

		if (network.internetconnected)
		{
			network.GetFileDates(_LoadFile_path);
			while (network.waitingnetwork)
				yield return new WaitForSeconds(0.03f);
			
			string [] allfiles = network.returntext.Split(';');
			
			for (int i=0;i<allfiles.Length;i+=2)
			{
				if (allfiles[i] == _LoadFile_filename)
				{
					int.TryParse(allfiles[i+1],out i_servertimedate);
					break;
				}
			}
			if (i_servertimedate < savedate)
			{
				network.PutFile(_LoadFile_path,_LoadFile_filename);
				while (network.waitingnetwork)
					yield return new WaitForSeconds(0.03f);

				// internet connected and out new file -> upload
			}
		}
		networkfilesbusy = false;
	}



	IEnumerator i_LoadFile()
	{
		string targetpath = localfilespath;
		byte [] data = null;
		int i_servertimedate = 0;

		System.IO.Directory.CreateDirectory(targetpath);
		targetpath = targetpath + "/" + _LoadFile_path;
		System.IO.Directory.CreateDirectory(targetpath);
		targetpath = targetpath + "/" + _LoadFile_filename;

		if (network.internetconnected)
		{
			network.GetFileDates(_LoadFile_path);
			while (network.waitingnetwork)
				yield return new WaitForSeconds(0.03f);

			string [] allfiles = network.returntext.Split(';');

			for (int i=0;i<allfiles.Length;i+=2)
			{
				if (allfiles[i] == _LoadFile_filename)
				{
					int.TryParse(allfiles[i+1],out i_servertimedate);
					break;
				}
			}
		}
		if (System.IO.File.Exists(targetpath) && System.IO.File.Exists(targetpath+"_date"))
		{
			string	localtimedate = System.IO.File.ReadAllText(targetpath+"_date");
			int		i_localtimedate;
			int.TryParse(localtimedate,out i_localtimedate);
// check date
			if ((i_servertimedate == 0) || (i_servertimedate <= i_localtimedate))
			{
				data = System.IO.File.ReadAllBytes(targetpath);
			}
		}

		if (i_servertimedate == 0)
		{
			// file does not exist
		}
		else
		{
			if (data == null)
			{
//				Debug.Log ("Load Server");
				// get online file and update versioning
//				Debug.Log("Get file "+_LoadFile_path+" "+_LoadFile_filename);
				network.GetFile(_LoadFile_path,_LoadFile_filename);
				while (network.waitingnetwork)
					yield return new WaitForSeconds(0.03f);
				if (network.returndata != null)
				{
//					Debug.Log ("Save local data");
					// save both files
					System.IO.File.WriteAllBytes(targetpath,network.returndata);
					System.IO.File.WriteAllText(targetpath+"_date",i_servertimedate.ToString());
				}
				data = network.returndata;
			}
		}
		Data = data;
//
		networkfilesbusy = false;
	}
}
