﻿using UnityEngine;
using System.Collections;

public class CreerServeur : buttonswap {
	public  GameObject creer;
	public  GameObject joindre;

	

	// Use this for initialization
	void Start () {

		creer = GameObject.Find ("PopUpCreerServeur") as GameObject;
		joindre = GameObject.Find ("PopUpJoindreServeur") as GameObject;
		creer.SetActive (false);
		joindre.SetActive (false);
	}

	public void join(){

		joindre.SetActive (true);
		creer.SetActive (false);
		joindre.transform.GetComponent<listeServeur> ().refresh = true;
	}
	
	// Update is called once per frame
	public override void ButtonPressed ()
	{
		base.ButtonPressed();

		creer.SetActive (true);
		joindre.SetActive (false);

	}
}
