﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NetworkManager : MonoBehaviour {
	public void envoyerscore ()
	{
		throw new System.NotImplementedException ();
	}

	private const string typeName = "PerfectIndustry_ChaineCritique";
	public  string gameName = "";
	public HostData[] hostList;
	
	//variable partagé
	public float tempsLocal;

	public bool spectateur;

	public float tmps;
	public bool start;
	public int nombreConnecte;
	public List<int> piles;
	public int numJoueur;
	public NetworkView netview;
	public int entree;
	public int sortie;
	public bool connected;
	public bool ready;
	public List<bool> pret;
	public List<bool> spectateurs;
	public List<bool> femmes;
	public List<bool> partis;
	public string nomjoueur;
	public List<string> noms;


	public List<int> ordre;
	public List<float> temps;
	public float tempsEntreSequence;
	public float tempsEntreInput;
	public float tempsPartie;
	public List<int> results;
	public List<int> scores;
	public int nombretotalcree;
	
	

	
	//test logique
	public bool rafraich ;
	public bool ajout;
	public bool etat;
	public bool commencer;
	public bool fin;
	public bool loaded;
	public bool resinit;
	public bool spectate;
	public bool creerPerso;
	public bool occulus;
	public bool femme;

	//graphe
	public float tempsGraphe;
	public List<List<int>> donneeGraph;
	

	//position autre personnage
	public List<Vector3> positions;
	public List<Quaternion> orientations;

	
	// Use this for initialization
	void Start () {
		femme = false;
		spectateur = false;
		spectate = false;
		creerPerso = false;
		GameObject.DontDestroyOnLoad( gameObject );
		netview = gameObject.transform.GetComponent<NetworkView>();
		tempsLocal = 1;
		entree = 0;
		sortie = 0;
		tmps = 0;
		nombretotalcree = 0;
		start = false;
		nombreConnecte = 0;
		tempsEntreSequence = 5;
		tempsEntreInput = 0.5f;
		tempsPartie = 120;
		tempsGraphe = -1;
		connected = false;
		piles = new List<int> ();
		ordre = new List<int> ();
		ready = false;
		rafraich = true;
		commencer = false;
		RefreshHostList ();
		fin = false;
		loaded = false;
		resinit=true;
		results = new List<int> ();
		scores = new List<int> ();
		donneeGraph = new List<List<int>> ();
		
		
	}
	
	// Update is called once per frame
	void Update () {

		int vb=0;
		foreach (bool b in femmes) {

			Debug.Log(noms[vb]+b);
			vb++;
		}
		//Debug.Log(network.
		if (GameObject.Find ("resultattotal") && fin) {
			GameObject.Find ("resultattotal").transform.GetComponent<tk2dTextMesh> ().text = nombretotalcree.ToString ();
		}
		if (Network.isServer && nombreConnecte < Network.connections.Length+1) {
			piles.Add (0);
			
			nombreConnecte++;
			if(nombreConnecte==1){
				pret.Add(false);
				spectateurs.Add(false);
				femmes.Add(false);
				partis.Add(false);
				positions.Add(new Vector3());
				orientations.Add(new Quaternion());
			}
			netview.RPC("raff",RPCMode.All);
			
		}
		
		if(Network.isServer ){
			
			pret[0]=true;
			bool testlogique=true;
			foreach(bool b in pret){
				if(!b){
					testlogique=false;
				}
			}
			ready=testlogique;
			if(commencer){

				if(tempsGraphe<0){
					tempsGraphe=5;
					donneeGraph.Add(piles);
				}
				else{
					tempsGraphe-=Time.deltaTime;
				}
				tempsPartie-=Time.deltaTime;
				if(tempsPartie<0){
					
					commencer=false;
					netview.RPC("fins",RPCMode.All);
				}
				
				entree=piles[numJoueur];
				sortie=piles[numJoueur+1];
				int l=0;
				foreach(float ft in temps){
					temps[l]+=Time.deltaTime;
					l++;
				}
			}
			if (tmps > tempsLocal && commencer) {
				tmps=0;
				piles[0]+=1;
				
			} else {
				tmps+=Time.deltaTime;
			}
			/*if(fin && loaded){
				
				int h=0;
				foreach(string n in noms){
					netview.RPC("menufin",RPCMode.Others,n,results[h]);
					h++;
				}
				nombretotalcree = results [ordre.IndexOf (nombreConnecte - 1)];
				netview.RPC ("totalcree",RPCMode.All,nombretotalcree);

				GameObject.Find ("results").transform.GetComponent<listeresusltats> ().setUpMenu ();
				fin=false;
			}*/
			int k=0;
			foreach(string n in noms){
				netview.RPC("miseajour",RPCMode.Others,n,pret[k],positions[k],orientations[k],femmes[k],partis[k]);
				k++;
			}
			
			
		}
		if(Network.isClient){

			if(spectate || commencer){
				if(!creerPerso) {
					Vector3 spawnPoint = GameObject.Find("spawn1").transform.position;
					//var hitColliders = Physics.OverlapSphere(spawnPoint, 2);//2 is purely chosen arbitrarly
					if(spawnLibre(spawnPoint)){
						GameObject nouv=new GameObject();
						GameObject.Find("Main Camera").SetActive(false);
						if(!femme){
							 nouv = Instantiate (Resources.Load ("personnageHomme")) as GameObject;
						}
						else{
							 nouv = Instantiate (Resources.Load ("personnageFemme")) as GameObject;
						}
						nouv.transform.position = spawnPoint;
						nouv.name=nomjoueur;
						creerPerso=true;
					}
				}
				else{

					netview.RPC("sendPosition",RPCMode.Server,noms.IndexOf(nomjoueur),GameObject.Find(nomjoueur).transform.position,GameObject.Find(nomjoueur).transform.rotation);
				}

			}

			if(commencer){
				netview.RPC("knowMyentree",RPCMode.Server,numJoueur);
			}
			if(commencer && resinit){
			
				for(int q=0;q<pret.Count;q++){
				
					results.Add (0);
				}
				resinit=false;
			}
			if(ajout){
				netview.RPC("addName",RPCMode.Server,nomjoueur);
				netview.RPC("raff",RPCMode.All);
				ajout=false;
			}
			if(etat){
				netview.RPC("Ready",RPCMode.Server,nomjoueur);
				netview.RPC("raff",RPCMode.All);
				etat=false;
			}
		}

		int m=0;
		foreach(string s in noms){
			if(partis[m]){
				if(s!=nomjoueur && GameObject.Find(s)){
				
					GameObject.Find(s).transform.position=positions[m];
					GameObject.Find(s).transform.rotation=orientations[m];
				}
				else if(!GameObject.Find(s) && s!=nomjoueur){
					GameObject nouv=new GameObject();
					if(!femmes[m]){
						nouv = Instantiate (Resources.Load ("autrePersonnage")) as GameObject;
					}
					else{
						nouv = Instantiate (Resources.Load ("autrePersonnageF")) as GameObject;
					}
					nouv.transform.position=positions[m];
					nouv.transform.rotation=orientations[m];
					nouv.name=s;
				
				}
			}
			m++;
		}

	}

	float Distance (Vector3 v, Vector3 tmp)
	{
		return Mathf.Sqrt ((v.x - tmp.x)*(v.x - tmp.x) + (v.y - tmp.y)*(v.y - tmp.y) + (v.z - tmp.z)*(v.z - tmp.z));
	}

	bool spawnLibre (Vector3 v)
	{
		foreach(Vector3 tmp in positions){
			if(Distance(v,tmp)<3){
				return false;
			}
		}
		return true;
	}
	
	public void StartServer()
	{
		Network.InitializeServer(10, 26000, true);
		MasterServer.RegisterHost(typeName, gameName);
		noms.Add (nomjoueur);
		
	}
	public void StartServerOcculus(){
		Network.InitializeServer(10, 25000, !Network.HavePublicAddress());
		MasterServer.RegisterHost(typeName, gameName);
		noms.Add (nomjoueur);
		occulus = true;

	}
	
	void OnServerInitialized()
	{
		Debug.Log("Server Initializied");
		//Application.LoadLevel( "SalleAttente" );
		if (!occulus) {
			Application.LoadLevel ("jeu3D");
		} else {
			Application.LoadLevel ("jeu3DOculus");
		}
	}
	
	
	
	public void RefreshHostList()
	{
		MasterServer.RequestHostList(typeName);
	}
	
	void OnMasterServerEvent(MasterServerEvent msEvent)
	{
		if (msEvent == MasterServerEvent.HostListReceived)
			hostList = MasterServer.PollHostList();
	}
	
	public void JoinServer(HostData hostData)
	{
	
		Debug.Log(hostData.ip[0]);
		Debug.Log (hostData.port);
		Network.Connect(hostData.guid);
		connected = true;
		//Application.LoadLevel( "SalleAttente" );
		Application.LoadLevel ("jeu3D");
		
		
	}
	
	void OnConnectedToServer()
	{
		
		Debug.Log("Server Joined");
		
	}

	[RPC]
	void sendPosition (int num,Vector3 pos,Quaternion ori)
	{
		
		positions [num] = pos;
		orientations [num] = ori;

	}

	[RPC]
	void knowMyentree (int num,NetworkMessageInfo info)
	{
		if (num != 100) {
			netview.RPC ("setPile", info.sender, piles [num], piles [num + 1]);
		}
	}
	
	[RPC]
	void setPile (int ent,int sort)
	{
		
		entree = ent;
		sortie = sort;
	}
	
	[RPC]
	void sendFinSequence (int numeroJoueur)
	{
		if (numeroJoueur != 100) {
			piles [numeroJoueur] -= 1;
			piles [numeroJoueur + 1] += 1;
			results [numeroJoueur] += 1;
		}
	}
	
	public void finsequence(){
		if (Network.isClient) {
			netview.RPC ("sendFinSequence", RPCMode.Server, numJoueur);
		} else {
			piles[numJoueur]-=1;
			piles[numJoueur+1]+=1;
			results[numJoueur]+=1;
		}
	}
	
	[RPC]
	void loadGame ()
	{


		Application.LoadLevel( "jeu" );
	}
	
	
	
	[RPC]
	void totalcree (int nombre)
	{
		if (loaded) {
			nombretotalcree = nombre;
			GameObject.Find ("results").transform.GetComponent<listeresusltats> ().setUpMenu ();
		}
		
	}
	
	[RPC]
	void menufin (string nomj,int res)
	{

		results[noms.IndexOf (nomj)] = res;
	}
	
	[RPC]
	void fins ()
	{
		
		fin = true;
		GameObject.Find (nomjoueur).GetComponent<AnimTriggers> ().sortie ();
		
		
		
		
	}
	
	public void launchGame(){
		int i = 0;
		int taille = noms.Count;
		int nbrspec = 0;
		foreach (string st in noms) {
			if(spectateurs[noms.IndexOf(st)]){
				nbrspec++;
				piles.Remove(0);
			}
		}
		tempsLocal=2;
		tempsEntreSequence=5;
		tempsPartie=120;
		int k = 0;
		foreach (string st in noms) {
				
			if(!spectateurs[noms.IndexOf(st)]){
				ordre.Add(-1);
				Debug.Log ("set pile avant");

				temps.Add(0);
				results.Add(0);
				if(k>0){
					Debug.Log ("set pile");
					int rand=(int)(Random.value*8);
					piles[k]=2+rand;
					//salut
				}
				else{
					piles[0]=1000;
				}
				k++;
			}
			else{
				ordre.Add(100);
			}
		}
		foreach (string st in noms) {
			if(!spectateurs[noms.IndexOf(st)]){
				int tmp=Random.Range(0,taille-i-nbrspec);	
				bool place=true;
				int j=0;
				while(place){
					if(ordre[j]<0){
						if(tmp==0){
						
							place=false;
							ordre[j]=i;
						}
						else{
							tmp-=1;
						}
					}
					else{
						j++;
					}
				}
				i++;
			}

		}

		piles.Add (0);
		netview.RPC("settemps",RPCMode.All,tempsEntreInput,tempsEntreSequence,tempsPartie);
		netview.RPC("setUpNumJoueur",RPCMode.All);
		commencer = true;
		netview.RPC("setUpScore",RPCMode.All);
		//netview.RPC("loadGame",RPCMode.All);
		
	}
	[RPC]
	void setUpScore ()
	{
		foreach (string st in noms) {
			scores.Add (-1);
		}
		GameObject.Find ("TableauScore").transform.GetComponent<listeScore> ().refresh = true;

		
	}
	[RPC]
	void settemps (float ti,float ts,float tp)
	{
		tempsEntreInput = ti;
		tempsEntreSequence = ts;
		tempsPartie = tp;
		
	}
	
	[RPC]
	void setUpNumJoueur ()
	{
		
		netview.RPC("asknum",RPCMode.Server,nomjoueur);
		
	}
	
	[RPC]
	void asknum (string nomj,NetworkMessageInfo info)
	{
		
		if (Network.isServer) {
			netview.RPC ("setnom", info.sender, ordre [noms.IndexOf (nomj)]);
		}
		
	}
	
	[RPC]
	void setnom (int num)
	{
		
		numJoueur = num;
		commencer = true;
		
	}
	[RPC]
	void changename (string nomj)
	{
		nomjoueur = nomj;
		netview.RPC ("addName", RPCMode.Server,nomjoueur);
		
	}
	[RPC]
	void addName (string nomj,NetworkMessageInfo info)
	{
		
		if(!noms.Contains(nomj)){
			
			noms.Add (nomj);
			pret.Add(false);
			spectateurs.Add(false);
			femmes.Add(false);
			partis.Add(false);
			positions.Add(new Vector3());
			orientations.Add(new Quaternion());
		}
		else{
			
			netview.RPC ("changename", info.sender,nomj+"*");
		}
		
		
	}
	
	public void ajouterNom(){
		ajout = true;
	}
	
	[RPC]
	void Ready (string nomj)
	{
		pret [noms.IndexOf (nomj)] = true;
		
	}
	public void changePerso(bool b){
		netview.RPC ("deviensFemme",RPCMode.Server ,nomjoueur,b);
	}

	[RPC]
	void deviensFemme(string nomj,bool b){
		femmes [noms.IndexOf (nomj)] = b;
	}
	public void go(){
		netview.RPC ("jlaunch",RPCMode.Server ,nomjoueur);
	}
	
	[RPC]
	void jlaunch(string nomj){
		partis [noms.IndexOf (nomj)] = true;
	}
	public void envoyerEtat(){
		etat = true;
	}
	
	[RPC]
	void miseajour (string nomautre,bool test,Vector3 pos,Quaternion ori,bool b,bool b1)
	{

		if(noms.Contains(nomautre)){
		
			if(pret [noms.IndexOf (nomautre)]!=test){

				pret [noms.IndexOf (nomautre)]=test;

				rafraich=true;
				
			}
			positions [noms.IndexOf (nomautre)]=pos;
			orientations[noms.IndexOf (nomautre)]=ori;
			femmes[noms.IndexOf (nomautre)]=b;
			partis[noms.IndexOf (nomautre)]=b1;
		}
		else{

			noms.Add(nomautre);
			pret.Add(test);
			positions.Add(pos);
			orientations.Add(ori);
			femmes.Add(b);
			partis.Add(b1);
			rafraich=true;

		}
		
	}
	
	[RPC]
	void newseq (int numJoueur,NetworkMessageInfo info)
	{


		if (piles [numJoueur] > 0 && temps [numJoueur] > tempsEntreSequence) {
			temps [numJoueur] = 0;

			netview.RPC ("startnewsequence", info.sender);
		} 
		
	}
	
	[RPC]
	void startnewsequence (NetworkMessageInfo info)
	{

		GameObject.Find (nomjoueur).transform.GetComponent<jeu3dSequence> ().restart ();
	}
	
	public void nouvellesequence ()
	{
		if (Network.isClient) {

			netview.RPC ("newseq", RPCMode.Server, numJoueur);
		} else {

			temps [numJoueur] = 0;
			GameObject.Find (nomjoueur).transform.GetComponent<jeu3dSequence> ().restart ();
		}
	}
	public void estSpectateur ()
	{
		netview.RPC ("specNom",RPCMode.Server,nomjoueur);
	}

	[RPC]
	void specNom (string nomj)
	{

		spectateurs [noms.IndexOf (nomj)] = true;
	}


	[RPC]
	void tableEtat (string nomTable)
	{
		GameObject.Find (nomTable).transform.GetComponent<isOccupied> ().occup=true;
	}

	public void etatTable (string nomTable)
	{
		netview.RPC ("tableEtat",RPCMode.All,nomTable);
	}
	
	[RPC]
	void raff ()
	{
		rafraich = true;
		
	}

	public void envoyerscore (int score)
	{
		netview.RPC ("scoreJoueur",RPCMode.All,nomjoueur,score);
	}
	
	[RPC]
	void scoreJoueur (string nom,int score)
	{
		GameObject.Find ("TableauScore").transform.GetComponent<listeScore> ().refresh = true;
		scores[noms.IndexOf(nom)]=score;
		
	}
	
	
	
	
	
	/*void OnGUI()
	{
		int w = Screen.width;
		int h = Screen.height;
		/*if (!Network.isClient && !Network.isServer)
		{
			if (GUI.Button(new Rect(100, 100, 250, 100), "Start Server"))
				StartServer();
			
			if (GUI.Button(new Rect(100, 250, 250, 100), "Refresh Hosts"))
				RefreshHostList();
			
			if (hostList != null)
			{
				for (int i = 0; i < hostList.Length; i++)
				{
					if (GUI.Button(new Rect(400, 100 + (110 * i), 300, 100), hostList[i].gameName)){
						JoinServer(hostList[i]);

					}
						
				}
			}
		}

		if (Network.isClient || Network.isServer) {
			int i=0;
			if (GUI.Button(new Rect((int)(w/5),(int)(1*h/12), (int)(3*w/5),(int)(h/6)), gameName)){}
			foreach (string nom in noms) {
				if(pret [i]){
					GUI.color=Color.green;
				}
				else{
					GUI.color=Color.red;
				}
				if(i<5){
					if (GUI.Button(new Rect((int)(w/5),(int)(3*h/12)+i*(int)(h/12), (int)(w/5),(int)(h/12)), nom)){}
				}
					

				i++;
		
			}
			GUI.color=Color.white;
			if (Network.isClient){
				if (GUI.Button(new Rect((int)(4*w/5),(int)(5*h/6), (int)(w/5),(int)(h/6)), "Pret")){
					envoyerEtat();
					GameObject.Find("Main Camera").SetActive(false);
					GameObject nouv = Instantiate (Resources.Load ("personnageHomme")) as GameObject;
					nouv.transform.position = GameObject.Find("spawn1").transform.position;
					nouv.name=nomjoueur;
				}
			}
			if (Network.isServer){
				if (GUI.Button(new Rect((int)(4*w/5),(int)(5*h/6), (int)(w/5),(int)(h/6)), "lancer")){
					
					launchGame();
				}
			}

		}
	}*/

	public void situationTable (string name, int gauche1, int gauche2, int centre, int droite1, int droite2, int entree, int sortie)
	{
		netview.RPC ("rafraichTable",RPCMode.Others,name,gauche1,gauche2,centre,droite1,droite2,entree,sortie);
	}

	[RPC]
	void rafraichTable(string nom,int g1,int g2,int c,int d1,int d2,int e, int s){
		Debug.Log (nom);
		GameObject table = GameObject.Find (nom);
		Debug.Log (g1);
			table.transform.GetChild(0).GetChild(0).GetComponent<tk2dSlicedSprite>().spriteId=g1;
			table.transform.GetChild(1).GetChild(0).GetComponent<tk2dSlicedSprite>().spriteId=g2;
			table.transform.GetChild(2).GetChild(0).GetComponent<tk2dSlicedSprite>().spriteId=c;
			table.transform.GetChild(3).GetChild(0).GetComponent<tk2dSlicedSprite>().spriteId=d1;
			table.transform.GetChild(4).GetChild(0).GetComponent<tk2dSlicedSprite>().spriteId=d2;
			table.transform.GetChild(5).GetChild(0).GetChild(0).GetComponent<tk2dTextMesh>().text=e.ToString();
			table.transform.GetChild(5).GetChild(0).GetChild(1).GetComponent<tk2dTextMesh>().text=s.ToString();


	}

	public void quitter ()
	{
		netview.RPC ("quitterServer",RPCMode.Server,nomjoueur);
		Network.Disconnect ();
		Application.LoadLevel ("menuServeur");

	}

	[RPC]
	void quitterServer(string nom){
		int index = noms.IndexOf (nom);
		noms.Remove (nom);
		pret.RemoveAt (index);
		spectateurs.RemoveAt (index);
		femmes.RemoveAt (index);
		partis.RemoveAt (index);
		positions.RemoveAt (index);
		orientations.RemoveAt (index);
		rafraich = true;
		netview.RPC ("quitterJoueur",RPCMode.Others,nomjoueur);
	}

	[RPC]
	void quitterJoueur(string nom){
		int index = noms.IndexOf (nom);
		noms.Remove (nom);
		pret.RemoveAt (index);
		spectateurs.RemoveAt (index);
		femmes.RemoveAt (index);
		partis.RemoveAt (index);
		positions.RemoveAt (index);
		orientations.RemoveAt (index);
		rafraich = true;
	
	}
}
