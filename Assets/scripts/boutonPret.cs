﻿using UnityEngine;
using System.Collections;

public class boutonPret : buttonswap {
	
	public GameObject networ;
	public GameObject colider;
	// Use this for initialization
	void Start () {
		colider = GameObject.Find ("ServeurTable");
		networ = GameObject.Find ("network");
		if (Network.isServer) {
			transform.GetChild(0).GetComponent<tk2dTextMesh>().text="lancer";
		}
	}
	
	public override void ButtonPressed ()
	{
		if (Network.isServer) {
			base.ButtonPressed (); 
			if(networ.transform.GetComponent<NetworkManager> ().ready){
				GameObject.Find("Main Camera").SetActive(false);
				GameObject nouv = Instantiate (Resources.Load ("personnageHomme")) as GameObject;
				Destroy(colider.GetComponent<BoxCollider>());
				nouv.transform.position = new Vector3(0,0,-0.5f);
				nouv.name=networ.transform.GetComponent<NetworkManager> ().nomjoueur;
				networ.transform.GetComponent<NetworkManager> ().envoyerEtat();

				nouv.GetComponent<BotControlScript>().animSpeed=0;
				nouv.GetComponent<BotControlScript>().lookSmoother=0;

			
				colider.transform.GetComponent<isOccupied>().occup=true;
				nouv.GetComponent<jeu3dSequence>().editcentre(colider);

				networ.transform.GetComponent<NetworkManager> ().launchGame();
				networ.transform.GetComponent<NetworkManager> ().go();
			}
		}
		if (Network.isClient) {
			base.ButtonPressed (); 
			//GameObject.Find("nom: "+networ.transform.GetComponent<NetworkManager> ().nomjoueur).transform.GetChild(0).GetComponent<tk2dTextMesh>().color=Color.green;
			//networ.transform.GetComponent<NetworkManager> ().envoyerEtat();
			networ.transform.GetComponent<NetworkManager> ().spectate=true;
			networ.transform.GetComponent<NetworkManager> ().go();
		}
	
	}

}
